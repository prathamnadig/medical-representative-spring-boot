package com.medrep.entity;

public class Drug {

	private int drugId;
	private String drugName;

	private int qty;
	private int price;

	public int getDrugId() {
		return drugId;
	}

	public void setDrugId(int drugId) {
		this.drugId = drugId;
	}

	public String getDrugName() {
		return drugName;
	}

	public void setDrugName(String drugName) {
		this.drugName = drugName;
	}

	public int getQty() {
		return qty;
	}

	public void setQty(int qty) {
		this.qty = qty;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public Drug() {

	}

	public Drug(int drugId, String drugName, int qty, int price) {
		this.drugId = drugId;
		this.drugName = drugName;
		this.qty = qty;
		this.price = price;

	}
}
