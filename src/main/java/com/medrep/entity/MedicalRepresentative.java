package com.medrep.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "MedicalRepresentative")
public class MedicalRepresentative {

	@Id
	private int id;

	@Column
	private String name;

	@Column
	private String drug;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public MedicalRepresentative() {

	}

	public MedicalRepresentative(int id, String name) {
		this.id = id;
		this.name = name;

	}
}
