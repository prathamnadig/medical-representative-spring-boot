package com.medrep.dao;

import java.util.List;

import com.medrep.entity.Drug;

public interface DrugDao {

	public List<Drug> getAllDrugs();
	
	public Drug addDrug();
	
	

}
