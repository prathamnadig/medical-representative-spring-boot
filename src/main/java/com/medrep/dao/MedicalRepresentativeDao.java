package com.medrep.dao;

import java.util.List;

import com.medrep.entity.MedicalRepresentative;

public interface MedicalRepresentativeDao {

	public void addMedicalRepresentative();

	public List<MedicalRepresentative> getAllRepresentatives();

	public MedicalRepresentative getRepresentativeByName(String name);
	public MedicalRepresentative getRepresentativeById(int id);
	public MedicalRepresentative updateDrugByRepresentativeName(String name);

	public void deleteByRepName(String name);

}
