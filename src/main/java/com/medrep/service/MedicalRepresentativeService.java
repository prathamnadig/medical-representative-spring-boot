package com.medrep.service;

import java.util.List;

import com.medrep.entity.MedicalRepresentative;

public interface MedicalRepresentativeService {
	public void addMedicalRepresentative();

	public List<MedicalRepresentative> getAllRepresentatives();

	public MedicalRepresentative getRepresentativeByName(String name);

	public MedicalRepresentative updateDrugByRepresentativeName(String name);

	public void deleteByRepName(String name);


}
