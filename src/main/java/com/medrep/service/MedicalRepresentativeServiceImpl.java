package com.medrep.service;

import java.util.List;

import com.medrep.dao.MedicalRepresentativeDaoImpl;
import com.medrep.entity.MedicalRepresentative;

public class MedicalRepresentativeServiceImpl implements MedicalRepresentativeService {

	MedicalRepresentativeDaoImpl MrDaoImpl = new MedicalRepresentativeDaoImpl();

	
	
	
	public void addMedicalRepresentative() {

		// TODO Auto-generated method stub

	}

	public List<MedicalRepresentative> getAllRepresentatives() {
		List<MedicalRepresentative> mlist = MrDaoImpl.getAllRepresentatives();
		return mlist;
	}

	public MedicalRepresentative getRepresentativeByName(String name) {
		MedicalRepresentative m=MrDaoImpl.getRepresentativeByName(name);
		return m;
	}

	public MedicalRepresentative updateDrugByRepresentativeName(String name) {
		MedicalRepresentative m=MrDaoImpl.updateDrugByRepresentativeName(name);
		return m;
	}

	public void deleteByRepName(String name) {
	

	}

	public MedicalRepresentative getRepresentativeById(int id) {
		MedicalRepresentative m=MrDaoImpl.getRepresentativeById(id);
		return m;
	}

	public MedicalRepresentative updateDrugByRepresentativeName(int id, String name, String drugName) {
		MedicalRepresentative m=MrDaoImpl.updateDrugByRepresentativeName(drugName);
		return m;
	}

}
