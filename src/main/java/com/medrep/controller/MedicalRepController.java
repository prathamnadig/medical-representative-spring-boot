package com.medrep.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.medrep.entity.MedicalRepresentative;
import com.medrep.service.MedicalRepresentativeServiceImpl;

@Controller

public class MedicalRepController {

	@Autowired
	public MedicalRepresentativeServiceImpl medicalRepService;

	@RequestMapping(value = "/MedicalRepresentatives")
	public List<MedicalRepresentative> getAllMedicalReps() {
		return medicalRepService.getAllRepresentatives();

	}

	
	@RequestMapping(value="/MedicalRepresentatives/{id}")
	public MedicalRepresentative getRepresentativeByName() {
		return medicalRepService.getRepresentativeById(id);
	}
}
